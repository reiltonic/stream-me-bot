package main

import (
	"fmt"
	"strings"
)

//FFMPEGOpus returns the ffmpeg command used to convert video/audio streams to opus audio stream
func FFMPEGOpus(volume float64, uri string) string {

	input := "-i pipe:0"
	if len(uri) > 0 {
		input = fmt.Sprintf(`-i "%s"`, uri)
	}

	cmd := []string{
		"ffmpeg",
		"-report",
		"-reconnect 1",
		//"-reconnect_at_eof 1",
		"-reconnect_streamed 1",
		//"-reconnect_delay_max 10",
		input,
		"-bufsize 9600k",
		//"-max_muxing_queue_size 99999",
		"-application voip",
		"-ac 2",
		"-ar 48000",
		"-c:a libopus",
		"-b:a 128k",
		"-vbr off",
		"-compression_level 10",
		fmt.Sprintf(`-filter:a "volume=%.2f, loudnorm"`, volume),
		"-map 0:a:0",
		"-f data pipe:1",
	}
	return strings.Replace(strings.Join(cmd, " "), "  ", " ", -1)
}
