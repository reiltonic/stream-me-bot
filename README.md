# Stream me discord bot
This discord bot converts stream.me live streams to opus audio and replays them on a discord voice channel.  

### Requirements
Linux OS (tested on ubuntu 16.04 and up) or Windows 10 with [mingw64](https://www.msys2.org/)  
Latest [FFMPEG](https://www.ffmpeg.org/) has to be installed and found from $PATH  

### Building
Install
* [golang binaries](https://golang.org/dl/)  
* [go dep - a golang package manager](https://github.com/golang/dep)  

Build
* `dep ensure`
* `go build`

Run
* Edit and run start.sh