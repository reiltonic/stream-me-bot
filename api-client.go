package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/tidwall/gjson"
)

const channelRequest = "https://www.stream.me/api-user/v2/%s/app/web/channel?requestedLocale=en"

var (
	//ErrChannelNotActive means channel is not broadcasting right now
	ErrChannelNotActive = errors.New("Channel is not broadcasting")
	//ErrChannelNotExist ...
	ErrChannelNotExist = errors.New("Channel does not exist")
)

//GetStreamURL sends a http request to stream.me API and returns the stream URL or an error
func GetStreamURL(streamer string) (string, error) {

	if IsActive {
		return "", nil
	}

	log.Println("Poll stream status...")

	uri := fmt.Sprintf(channelRequest, streamer)
	client := http.DefaultClient

	r, err := client.Get(uri)
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return "", err
	}

	if len(body) == 0 {
		return "", nil
	}

	var isActive, manifestURL string
	streams := gjson.Get(string(body), "_embedded.streams")
	streams.ForEach(func(key, value gjson.Result) bool {
		isActive = value.Get("active").String()
		manifestURL = value.Get("manifest").String()
		return false
	})

	if IsActive == true && isActive == "true" {
		return "", nil
	}

	if isActive == "false" {
		return "", ErrChannelNotActive
	}

	if isActive == "" {
		return "", ErrChannelNotExist
	}

	if manifestURL == "" {
		return "", ErrChannelNotExist
	}

	encodings, err := getEncodings(manifestURL)
	if err != nil || encodings == nil {
		return "", err
	}

	bestEnc := encodings.BestAudio()
	if bestEnc.AudioKbps == 0 {
		return "", errors.New("No audio found")
	}

	log.Printf("Best audio bitrate: %d", bestEnc.AudioKbps)

	return bestEnc.Location, nil
}

func getEncodings(manifestURL string) (Encodings, error) {

	r, err := http.DefaultClient.Get(manifestURL)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	encodings := Encodings{}
	encodingsJSON := gjson.Get(string(body), "formats.mp4-hls.encodings")
	encodingsJSON.ForEach(func(key, value gjson.Result) bool {
		var enc Encoding
		//log.Println(value.String())
		err := json.Unmarshal([]byte(value.String()), &enc)
		if err != nil {
			log.Println(err)
			return true
		}
		encodings = append(encodings, enc)
		return true
	})

	return encodings, nil
}

//Encoding is a container for stream.me encoding object
type Encoding struct {
	VideoWidth  int    `json:"videoWidth"`
	VideoHeight int    `json:"videoHeight"`
	VideoKbps   int    `json:"videoKbps"`
	AudioKbps   int    `json:"audioKbps"`
	Location    string `json:"location"`
}

//Encodings is a slice of Encoding structs
type Encodings []Encoding

//BestAudio returns the best audio stream found from encodings
func (ecs Encodings) BestAudio() Encoding {
	best := Encoding{AudioKbps: 0}
	for _, e := range ecs {
		if e.AudioKbps > best.AudioKbps {
			best = e
		}
	}
	return best
}
