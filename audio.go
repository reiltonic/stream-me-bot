package main

import (
	"bufio"
	"encoding/binary"
	"errors"
	"io"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"time"
)

type AudioChan struct {
	Buffer       chan []byte
	Finished     bool
	BytesWritten int
	BytesRead    int
	Bitrate      int
	Byterate     float64
	StartOffset  int
	KillChan     chan bool
	Channels     int
	Framerate    int
	FrameSize    int
	BufferSize   int
}

func (a *AudioChan) Write(b []byte) (num int, err error) {
	if a.Finished {
		return 0, errors.New("cannot write to closed channel")
	}

	a.Buffer <- b
	num = len(b)
	a.BytesWritten += num
	return num, err
}

//Close closes the audio channel and sets .Finished to true
func (a *AudioChan) Close() error {
	if a.Finished {
		return errors.New("cannot close a closed channel")
	}
	a.Finished = true
	//a.Buffer <- a.byteBuffer

	close(a.Buffer)
	log.Println("audiochan CLOSED", len(a.Buffer))
	return nil
}

//NewBuffer swaps the current buffer with a new one
func (a *AudioChan) NewBuffer() {
	a.Buffer = make(chan []byte, a.BufferSize)
}

//Flush overwrites the current buffer with an empty one
func (a *AudioChan) Flush() {
	a.Buffer = make(chan []byte)
}

func (a *AudioChan) Read(b []byte) (num int, err error) {

	if len(a.Buffer) == 0 && a.Finished {
		log.Print("!EOF!")
		return 0, io.EOF
	}

	in := <-a.Buffer
	copy(b, in)

	a.BytesRead += len(in)

	return len(in), nil
}

//NewAudioChannel - Creates and returns a new audio channel.
func NewAudioChannel(bitrate, startOffset, channels, framerate, frameSize, bufferSize int) AudioChan {
	//buffer size in bytes is frameSize*channels*chunkSize (960*2*480)
	log.Printf("Buffer size is %d * %d bytes", frameSize, bufferSize)
	return AudioChan{
		Buffer:      make(chan []byte, bufferSize),
		KillChan:    make(chan bool),
		Bitrate:     bitrate,
		Byterate:    float64(bitrate*1024) / 8.0,
		StartOffset: startOffset,
		Channels:    channels,
		Framerate:   framerate,
		FrameSize:   frameSize,
		BufferSize:  bufferSize,
	}
}

func StreamToOpus(aChan *AudioChan, ffmpegCommand string) {
	var cmd *exec.Cmd
	defer func(cmd *exec.Cmd) {
		if cmd == nil {
			return
		}

		if err := kill(cmd); err != nil {
			log.Println("Kill command error: ", err)
		}
	}(cmd)
	log.Println("Starting ffmpeg")
	cmd = exec.Command("bash", "-c", ffmpegCommand)

	stdout, _ := cmd.StdoutPipe()

	go writeChannel(stdout, aChan, cmd)

	err := cmd.Start()
	if err != nil {
		log.Print("Failed to run ffmpeg", err)
		return
	}

	for !aChan.Finished {
		time.Sleep(time.Millisecond * 100)
	}
	err = kill(cmd)
}

func kill(cmd *exec.Cmd) error {

	if runtime.GOOS == "windows" {
		kill := exec.Command("TASKKILL", "/T", "/F", "/PID", strconv.Itoa(cmd.Process.Pid))
		kill.Stderr = os.Stderr
		kill.Stdout = os.Stdout
		return kill.Run()
	}
	//In case of unix, kill the process and wait for OS to read the exit signal,
	//otherwise they will linger in processes.
	cmd.Process.Kill()
	return cmd.Wait()
}

func writeChannel(stdout io.ReadCloser, aChan *AudioChan, cmd *exec.Cmd) {
	forciblyEnded := false
	defer func() {
		if !forciblyEnded {
			r := recover()
			aChan.Close()
			log.Println("writeChannel recover after panic", r)
		} else {
			aChan.Finished = true
			aChan.Flush()
		}
		kill(cmd)
	}()

	reader := bufio.NewReaderSize(stdout, aChan.FrameSize)
	for {
		select {
		default:
			audiobuf := make([]byte, aChan.FrameSize)
			err := binary.Read(reader, binary.BigEndian, &audiobuf)
			if err == io.EOF || err == io.ErrUnexpectedEOF {
				log.Println("Reader EOF: ", err)
				log.Println("Can still read: ", reader.Buffered())
				return
			}

			if err != nil {
				log.Println("Reader unexpected error: ", err)
				return
			}

			aChan.Write(audiobuf)

			if err != nil {
				log.Print("File writing error: ", err)
			}

		//if KillChan is not empty, forcibly destroy channel and ffmpeg process
		case forciblyEnded = <-aChan.KillChan:
			return
		}

	}
}

func readStream(stdin io.WriteCloser, body io.Reader) {
	defer stdin.Close()

	n, err := io.Copy(stdin, body)
	if err != nil {
		log.Print("Error reading stream ", err)
		return
	}
	log.Printf("Finished reading %d bytes", n)
}
