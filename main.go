/*
*	This discord bot will play audio of stream.me streams on a discord voice channel.
*	Currently supports LIVE streams only.
*	This bot requires a linux OS and FFMPEG (latest) has to be installed.
 */

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

var (
	BotToken        string
	ChannelName     string
	GuildID         string
	VoiceID         string
	VoiceConnection *discordgo.VoiceConnection
	AudioChannel    *AudioChan
	IsActive        = false
)

func main() {

	flag.StringVar(&BotToken, "token", "NTIzODMyNDg3MDU5OTE0NzUy.DvfREg.u7LAkCIejqSsIkVQ-RZSYcewF90", "Bot token. REQUIRED")
	flag.StringVar(&GuildID, "server", "399134408621424652", "Discord server ID. REQUIRED")
	flag.StringVar(&ChannelName, "channel", "npcanon88", "Stream.me channel NAME. default: npcanon88")
	flag.StringVar(&VoiceID, "voice", "442448624379625472", "Discord voice channel ID. Leave empty to connect to the first voice channel.")

	flag.Parse()

	fmt.Printf("\n\nToken: %s\nChannel name: %s\nGuild ID: %s\n\n", BotToken, ChannelName, GuildID)

	_, err := GetStreamURL(ChannelName)
	if err == ErrChannelNotExist {
		log.Fatalf("Channel %s does not exist.", ChannelName)
	}

	//start listening to discord events
	go DiscordListener(BotToken)

	for VoiceConnection == nil {
		time.Sleep(time.Millisecond * 300)
	}

	//start polling stream.me channel state
	go pollChannelState()

	//wait for ctrl + c
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

//pollChannelState polls the stream.me channel for changes on stream state
func pollChannelState() {
	aChan := AudioChan{}
	for {

		if aChan.Finished && len(aChan.Buffer) == 0 {
			IsActive = false
		}

		uri, err := GetStreamURL(ChannelName)
		if uri != "" {
			log.Println("Stream URL: ", uri)
		}

		if err == nil && !IsActive && uri != "" {
			log.Println("Broadcast started")
			IsActive = true
			aChan = NewAudioChannel(128, -1, 2, 48000, 320, 1000)
			go playback(uri, &aChan)
		}

		if err == ErrChannelNotActive && IsActive {
			IsActive = false
			log.Println("Broadcast ended")
		}

		time.Sleep(time.Second * 5)
	}
}

//playback loops through the audio channel buffer until channel exists
func playback(uri string, aChan *AudioChan) {

	err := VoiceConnection.Speaking(true)
	if err != nil {
		log.Fatal(err)
	}

	go StreamToOpus(aChan, FFMPEGOpus(0.5, uri))
	for buff := range aChan.Buffer {
		VoiceConnection.OpusSend <- buff
	}

	log.Println("Playback exiting...")
}
