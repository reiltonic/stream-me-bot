package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

//DiscordListener listens for discord events
func DiscordListener(token string) {

	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Fatal("Error creating Discord session", err)
		return
	}

	//Ready handler is triggered when the bot is ready.
	dg.AddHandler(Ready)
	//No need to handle messages since there are no commands atm.
	//dg.AddHandler(MessageCreate)
	dg.AddHandler(GuildCreate)

	// Open websocket connection to discord and begin listening.
	err = dg.Open()
	if err != nil {
		log.Fatal("error opening connection", err)
	}

	log.Print("Listening for discord messages!")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

func Ready(s *discordgo.Session, r *discordgo.Ready) {
	log.Println("READY EVENT")
}

//GuildCreate triggers every time the bot is added to a new server AND once per server on startup.
func GuildCreate(s *discordgo.Session, g *discordgo.GuildCreate) {
	log.Printf("GUILD CREATE EVENT\nName: %s\nID: %s\n", g.Name, g.ID)
	if g.Guild.ID != GuildID {
		return
	}

	for _, c := range g.Channels {
		var err error
		//if voice channel and user has not specified voice channel id, connect to the first available
		if c.Type == discordgo.ChannelTypeGuildVoice && VoiceID == "" {
			VoiceConnection, err = s.ChannelVoiceJoin(g.ID, c.ID, false, true)
			if err != nil {
				continue
			}
			return
		}

		//if voice channel has been set by the user, loop until the correct channel is found
		if c.ID == VoiceID && c.Type == discordgo.ChannelTypeGuildVoice {
			VoiceConnection, err = s.ChannelVoiceJoin(g.ID, c.ID, false, true)
			if err != nil {
				log.Fatal(err)
			}
			log.Println("Connected to voice channel!")
		}
	}
	if VoiceConnection == nil {
		log.Println("Could not connect to ANY voice channels!")
	}
}

func GetMessageOrigin(s *discordgo.Session, chID, authorID string) (guild *discordgo.Guild, vcID string, err error) {

	ch, err := s.State.Channel(chID)
	if err != nil {
		return
	}

	guild, err = s.State.Guild(ch.GuildID)
	if err != nil {
		return
	}

	for _, vs := range guild.VoiceStates {
		if vs.UserID == authorID {
			vcID = vs.ChannelID
		}
	}

	return
}

//MessageCreate is triggered for every discord message the bot can see, if handler is set.
func MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	/* 	if m.Author.ID == s.State.User.ID {
		return
	} */

	//Ignore all messages written by bots (inlcuding this one)
	if m.Author.Bot {
		return
	}

	guild, _, err := GetMessageOrigin(s, m.ChannelID, m.Author.ID)
	if err != nil {
		log.Println("MessageCreate: ", err)
		return
	}
	log.Println("MessageCreate guild ID: ", guild.ID)
	if guild.ID == GuildID {
		//Handle message
	}
}
